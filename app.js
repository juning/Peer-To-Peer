var settings = require('./settings');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var db = require('./models/db');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
global.config = require('./config.js');   //全局配置

// 初始化peerServer
var PeerServer = require('peer').PeerServer;
var _peerServer = PeerServer({port: 9000, path: '/ptpApi'});

var index = require('./routes/index');
var login = require('./routes/login');
var portal = require('./routes/portal');
var wallpaperAPI = require('./routes/wallpaperHandle');
var about = require('./routes/about');
var upload = require('./routes/upload');

//定义mongoStore,用于socket取得session值
var mongoStore = new MongoStore({
    db: settings.db,
    host: settings.host,
    port: settings.port
  });

var app = express();
app.enable('trust proxy');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//app.use('/public', express.static(__dirname + '/web-src'));

// 格式化输出
if (app.get('env') === 'development') {
  app.locals.pretty = true;
}

app.use(session({
  secret: settings.cookieSecret,
  key: settings.db,//cookie name
  cookie: {maxAge: 1000 * 60 * 60 * 24 * 30},//30 days
  resave: true,
  saveUninitialized: true,
  store: mongoStore
}));
// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'web-src')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'upload')));

app.use('/', login);
app.use('/login', login);
app.use('/index', index);
app.use('/portal', portal);
app.use('/wallpaperAPI', wallpaperAPI);
app.use('/about', about);
app.use('/upload', upload);


// peerServer Handler
_peerServer.on('connection', function(id) {
  console.log(id, 'connection');
});

_peerServer.on('disconnect', function(id) {
  console.log(id, 'disconnect');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  // respond with html page
  res.render('404');
  //if (req.accepts('html')) {
  //  res.render('404');
  //  return;
  //}
  //next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

// 创建chat实例；mongoStore在上边定义
var chat = require("./models/chat.js")(mongoStore);


module.exports = app;
