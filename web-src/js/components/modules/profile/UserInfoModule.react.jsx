var React = require('react');

require('../../../../less/profile.less')
var ModuleStore = require('../../../stores/ModuleStore');
var ModuleActions = require('../../../actions/ModuleActions');
var classnames = require('classnames');

var UserPhoto = React.createClass({
    getInitialState: function () {
        return {
            photoUrl: null
        };
    },
    componentDidMount: function () {
        // get user photo url
    },
    render: function () {
        return (
            <img src={this.state.photoUrl ? this.state.photoUrl : '/images/profile/default_avatar.png'}
                 className='avatar center-block img-circle'></img>
        );
    }
});

module.exports = React.createClass({
    getInitialState: function () {
        return {
            editing: false,
            descValue: null
        };
    },
    toggleEdit: function () {
        if (this.state.editing) {
            //TODO 保存
        }
        this.setState({
            editing: !this.state.editing
        });
    },
    handleChange: function (e) {
        this.setState({
            descValue: e.target.value
        });
    },
    render: function(){
        var editDescCls = classnames({'edit-desc': !this.state.editing}, 'btn', 'center-block', 'btn-xs', {'btn-link': !this.state.editing}, {'btn-success': this.state.editing});
        return (
            <div className="leftPart">
                <div className="container-fluid">
                    <UserPhoto/>
                    <h3 className='text-center'>{this.props.userInfo.name}</h3>
                    <div onClick={this.toggleEdit} className={editDescCls}>{this.state.editing ? '保存' : '编辑个人资料'} <span className="glyphicon glyphicon-pencil" aria-hidden="true"></span></div>
                    <p className={this.state.editing ? 'hidden' : ''}>{this.state.descValue ? this.state.descValue : this.props.userInfo.desc}</p>
                    <textarea defaultValue={this.state.descValue ? this.state.descValue : this.props.userInfo.desc}
                              className={this.state.editing ? 'form-control' : 'hidden'} onChange={this.handleChange} />
                    <div className='row'>
                        <div className="col-md-6">关注 <span className='counter'>{this.props.userInfo.countTwtter}</span></div>
                        <div className="col-md-6">收藏 <span className='counter'>{this.props.userInfo.countLike}</span></div>
                    </div>
                </div>
            </div>
        );
    }
});