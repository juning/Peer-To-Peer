var React = require('react');

var tweenState = require('react-tween-state');

module.exports = React.createClass({
    mixins: [tweenState.Mixin],
    getInitialState: function () {
        return {
            opacity: 0
        };
    },
    componentDidMount: function(){
        var that = this;
        that.tweenState('opacity', {
            easing: tweenState.easingTypes.easeInOutQuad,
            duration: 500,
            endValue: 1
        });
    },
    render: function(){
        var styleObj = {
            opacity: this.getTweeningValue('opacity'),
        };
        return (
            <div className='full-mask' style={styleObj}>
                <div className='static-container container'>{this.props.children}</div>
            </div>
        );
    }
});