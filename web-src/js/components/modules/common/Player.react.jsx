var React = require('react');

require('../../../../less/player.less');

var Glyphicon = require('react-bootstrap/lib/Glyphicon');

Number.prototype.toVideoDuration = function () {
    var hours, minutes, seconds, group;
    group = [];

    hours = Math.floor(this / 3600);
    minutes = Math.floor(this % 3600 / 60);
    seconds = Math.floor(this % 3600 % 60);

    if (hours > 0) {
        group.push((hours > 9) ? hours : "0" + hours);
    }
    group.push((minutes > 9) ? minutes : "0" + minutes);
    group.push((seconds > 9) ? seconds : "0" + seconds);

    return group.join(":");
};

var VideoFullScreenToggleButton = React.createClass({
    requestFullscreen: function () {
        this.props.onToggleFullscreen();
    },
    render: function () {
        return (
            <div className="toggle-fullscreen-button" onClick={this.requestFullscreen}>
                <Glyphicon glyph='resize-full'/>
            </div>
        );
    }
});

var VideoTimeIndicator = React.createClass({
    render: function () {
        var current = (this.props.currentTime).toVideoDuration();
        var duration = (this.props.duration).toVideoDuration();
        return (
            <div className="time">
                <span className="current">{current}</span>/<span className="total">{duration}</span>
            </div>
        );
    }
});

var VideoVolumeButton = React.createClass({
    toggleVolume: function () {
        this.props.toggleVolume(!this.props.muted);
    },
    changeVolume: function (e) {
        this.props.changeVolume(e.target.value);
    },
    render: function () {
        var volumeLevel = this.props.volumeLevel, level;
        if (volumeLevel <= 0) {
            level = 'muted';
        } else if (volumeLevel > 0 && volumeLevel <= 0.5) {
            level = 'low';
        } else {
            level = 'high';
        }

        var sound_levels = {
            'muted': 'volume-off',
            'low': 'volume-down',
            'high': 'volume-up'
        }

        return (
            <div className="volume">
                <span onClick={this.toggleVolume}>
                    <Glyphicon glyph={sound_levels[level]}/>
                </span>
                <input className="volume-slider" type="range" min="0" max="100" onInput={this.changeVolume}/>
            </div>
        );
    }
});

var VideoPlaybackToggleButton = React.createClass({
    render: function () {
        var icon = this.props.playing ? (<Glyphicon glyph='pause'/>) : (<Glyphicon glyph='play'/>);
        return (
            <div className="toggle-playback" onClick={this.props.handleTogglePlayback}>
                {icon}
            </div>
        );
    }
});

var VideoProgressBar = React.createClass({
    render: function () {
        var playedStyle = {width: this.props.percentPlayed + '%'}
        var bufferStyle = {width: this.props.percentBuffered + '%'}
        return (
            <div className="progress-bar">
                <div className="buffer-percent" style={bufferStyle}></div>
                <div className="playback-percent" style={playedStyle}></div>
            </div>
        );
    }
});

var Video = React.createClass({
    updateCurrentTime: function (times) {
        this.props.currentTimeChanged(times);
    },
    updateDuration: function (duration) {
        this.props.durationChanged(duration);
    },
    playbackChanged: function (shouldPause) {
        this.props.updatePlaybackStatus(shouldPause);
    },
    updateBuffer: function (buffered) {
        this.props.bufferChanged(buffered);
    },
    componentDidMount: function () {
        var video = this.getDOMNode();

        var $this = this;

        // Sent when playback completes
        video.addEventListener('ended', function (e) {
            $this.playbackChanged(e.target.ended);
        }, false);

        video.addEventListener('progress', function(){
            if (video.buffered.length === 0) return;
            var percent = (video.buffered.end(0) / video.duration * 100)
            $this.updateBuffer(percent);
        }, false);

        video.addEventListener('durationchange', function (e) {
            $this.updateDuration(e.target.duration);
        }, false);

        video.addEventListener('timeupdate', function (e) {
            $this.updateCurrentTime({
                currentTime: e.target.currentTime,
                duration: e.target.duration
            });
        }, false)
    },
    render: function () {
        return (
            <video src={this.props.url} poster={this.props.poster}></video>
        );
    }
});

var Player = React.createClass({
    getInitialState: function () {
        return {
            playing: false,
            percentPlayed: 0,
            percentBuffered: 0,
            duration: 0,
            currentTime: 0,
            muted: false,
            volumeLevel: 0.8,
            fullScreen: false
        };
    },
    videoEnded: function () {
        this.setState({
            percentPlayed: 100,
            playing: false
        });
    },
    togglePlayback: function (e) {
        this.setState({
            playing: !this.state.playing
        }, function () {
            if (this.state.playing) {
                this.refs.video.getDOMNode().play()
            } else {
                this.refs.video.getDOMNode().pause()
            }
        });
    },
    updateDuration: function (duration) {
        this.setState({duration: duration});
    },
    updateBufferBar: function (buffered) {
        this.setState({percentBuffered: buffered});
    },
    updateProgressBar: function (times) {
        var percentPlayed = Math.floor((100 / times.duration) * times.currentTime);
        this.setState({
            currentTime: times.currentTime,
            percentPlayed: percentPlayed,
            duration: times.duration
        });
    },
    toggleMute: function (e) {
        this.setState({
            muted: !this.state.muted
        }, function () {
            this.refs.video.getDOMNode().muted = this.state.muted
        });
    },
    toggleFullscreen: function (e) {
        this.setState({
            fullScreen: !this.state.fullScreen
        }, function () {
            if (this.state.fullScreen) {
                this.getDOMNode().webkitRequestFullScreen();
            } else {
                document.webkitExitFullscreen();
            }
        });
    },
    handleVolumeChange: function (value) {
        this.setState({volumeLevel: value / 100}, function () {
            this.refs.video.getDOMNode().volume = this.state.volumeLevel;
        });
    },
    render: function () {
        return (
            <div className="player-mask">
                <div className="player-container">
                    <Video ref="video"
                           url={this.props.options.url}
                           volume={this.state.volumeLevel}
                           poster={this.props.options.poster}
                           currentTimeChanged={this.updateProgressBar}
                           durationChanged={this.updateDuration}
                           updatePlaybackStatus={this.videoEnded}
                           bufferChanged={this.updateBufferBar}/>

                    <div className="video-controls" ref="videoControls">
                        <VideoProgressBar percentPlayed={this.state.percentPlayed}
                                          percentBuffered={this.state.percentBuffered}/>
                        <VideoPlaybackToggleButton handleTogglePlayback={this.togglePlayback}
                                                   playing={this.state.playing}/>
                        <VideoTimeIndicator duration={this.state.duration} currentTime={this.state.currentTime}/>
                        <VideoVolumeButton muted={this.state.muted} volumeLevel={this.state.volumeLevel}
                                           toggleVolume={this.toggleMute} changeVolume={this.handleVolumeChange}/>
                        <VideoFullScreenToggleButton onToggleFullscreen={this.toggleFullscreen}/>
                    </div>
                    <div className="close-btn">
                        <Glyphicon glyph='remove' onClick={this.props.hidePlayer}/>
                    </div>
                </div>
            </div>
        );
    }
});


module.exports = Player;
