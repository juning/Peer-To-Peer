/**
 * 为了方便以后做ajax请求统一入口，使用rewest不要直接require('reqwest')，而是使用如下用法
 * <pre><code>
 *     //import
 *     var when = require('when')
 *     var ajax = require('./services/ReqwestUtil.js');
 *
 *     //use
 *      when(ajax('/path')).then(fun(resp){
 *          console.log(resp);
 *      })
 * </code></pre>
 */

var reqwest = require('reqwest');

module.exports = reqwest;
