var keyMirror = require('keymirror');

module.exports = {
    events:keyMirror({
        CHANGE_CURRENT_MODULE: 'CHANGE_CURRENT_MODULE'
    }),
    modules: {
        home: {
            id: 'home',
            label: '首页'
        },
        profile: {
            id: 'profile',
            label: '主页'
        },
        movies: {
            id: 'movies',
            label: '电影'
        },
        chat: {
            id: 'chat',
            label: '聊天'
        },
        settings: {
            id: 'settings',
            label: '设置'
        }
    }
};