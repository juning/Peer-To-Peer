/*模拟模态框*/
(function($){
	$.fn.extend({
		leanModal:function(options){
			var defaults={
				top:100,
				overlay:0.5,
				closeButton:null,
    			callback:null
			};
			if($("#lean_overlay")[0]==undefined){
				var overlay=$("<div id='lean_overlay'></div>");
				$("body").append(overlay);
				$("#lean_overlay").css({
					"position":"fixed",
					"height":"100%",
					"width":"100%",
					"display":"none",
					"z-index":100,
					"top":0,"left":0,
					"background":"#000"
				});
			}
			options=$.extend(defaults,options);		
			return this.each(function(){
				var o=options;						
				$(this).click(function(e){
					var modal_id=$(this).attr("modal");
					$("#lean_overlay").click(function(){
						close_modal(modal_id);
					});
					$("[modal-toggle='show']").css({"display":"none"});
					$(o.closeButton).click(function(){
						close_modal(modal_id);
					});
					var modal_height=$(modal_id).outerHeight();
					var modal_width=$(modal_id).outerWidth();
					$("#lean_overlay").css({"display":"block",opacity:0});
					$("#lean_overlay").fadeTo(200,o.overlay);
					$(modal_id).attr("modal-toggle","show");		
					//$(modal_id).css({"display":"block","position":"fixed","opacity":0,"z-index":11000,"left":50+"%","margin-left":-(modal_width/2)+"px","top":o.top+"px"});
					$(modal_id).css({"display":"block","position":"fixed","opacity":0,"z-index":11000,"left":50+"%","margin-left":-(modal_width/2)+"px","top":50+"%","margin-top":-(modal_height/2)+"px"});
					$(modal_id).fadeTo(200,1);
					//e.preventDefault();
    		        if(o.callback!=null){o.callback()};
				})
			});
			function close_modal(modal_id){
				$("#lean_overlay").fadeOut(200);
				$(modal_id).css({"display":"none"});
				$("[modal-toggle='show']").css({"display":"none"});
			}
		}
	})
})(jQuery);
/* 输入框填充默认显示内容 */
$(document).ready(function(){ 
	
	$("[input-data-default]").each(function(){
        $(this).val("");
		var obj=$(this);obj.css({"z-index":"2","background":"none"});
		var title=obj.attr("input-data-default");
		obj.after("<span>"+title+"</span>");
		obj.next().css({"z-index":"1","position":"absolute","left":obj.css("left"),"top":obj.css("top"),"line-height":obj.css("line-height"),"color":"#aaa"});
		obj.bind("blur",function(){ 
			if(obj.val()==""){obj.next().html(title);}
		});
		obj.bind("keyup",function(){ 
			if(obj.val()!=""){obj.next().html("");}
			else{obj.next().html(title);}
		});
	});
})