/*首页背景图获取*/
$(function() {
	// get wallpaper from Bing
	var screenResolution = window.screen.width + 'x' + window.screen.height;
	$.get('wallpaperAPI', {screenResolution: screenResolution}, function(data) {
		var wpDiv = $('.wallpaper');
		var img = new Image();
		img.src = data;
		$(img).on('load', function() {
			$('.lg-container').fadeIn();
			wpDiv.css({
				"background-image" : 'url(' + img.src + ')'
			}).animate({
				opacity: 1
			}, 500, function(){});
		});
	});

	/*扩展jQuery 仅供Login 页面使用*/
	$.fn.extend({
		//检测邮箱
		checkEmail:function(){
			var email	=$(this).val();
			var regEmail=	/^[0-9a-zA-Z][0-9a-zA-Z_\.]*\@[0-9a-zA-Z]+(\.[a-zA-Z]{2,4})+$/;
			return regEmail.test(email) ;
		},
		//登陆表单Email后的提示
		loginEmailTip:function(res){
			var tips	=	$(".login-email-tip");
			if(res){
				tips.removeClass("error");tips.addClass("right");return true;
			}else{
				tips.removeClass("right");tips.addClass("error");return false;
			}
		},
		//注册表单Email后的提示
		regEmailTip:function(res,cls){
			var tips	=	$(cls);
			if(res){
				tips.removeClass("error");tips.addClass("right"); return true;
			}else{
				tips.removeClass("right");tips.addClass("error"); return false;
			}
		}
	});
	//登陆表单添加事件
	$("#loginEmail").blur(function(e){
		var res     =	$(this).checkEmail();
		return $(this).loginEmailTip(res);
	});
	//注册表单添加事件
	$("#regEmail").blur(function(e){
		var res	=	$(this).checkEmail();
		$(this).regEmailTip(res,".reg-email-tip1");
		if($("#regConfirmEmail").val()){return $(this).regEmailTip($("#regConfirmEmail").val()==$(this).val()&&$("#regConfirmEmail").checkEmail(),".reg-email-tip2");}
	});
	$("#regConfirmEmail").blur(function(e){
		var res     =	$(this).checkEmail();
		var bool    =   $(this).regEmailTip(res,".reg-email-tip2")&&$("#regEmail").val()==$(this).val()&&$(this).val()!="";
		return $(this).regEmailTip(bool,".reg-email-tip2");
	});
	$("#regPsw").blur(function(e){
		var psw=$(this).val();
		if(psw.length>=6){return $(this).regEmailTip(1,".reg-psw-tip");}
		else{return ($(this).regEmailTip(0,".reg-psw-tip"));}
	});

	// 注册新用户
	$('#regForm').on('submit', function(e) {
		e.preventDefault();
		var _userEmail = $('#regEmail').val();
		var _userPassword = $('#regPsw').val();
		var _captionText = $('#checkCode').val();
		if(validateRegForm()) {
			$.post('login/adduser', {
				useremail : _userEmail,
				password : _userPassword,
				captionText: _captionText
			}, function(body) {
				if(body.success) {
					resetForm($('#regForm'));
					setTimeout(function() {
						$("#gologin").trigger('click');
					}, 1000);
				} else {
					alert(body.msg);
				}
			}, 'json');
		} else {
			return;
		}
	});

	// 登陆
	$("#loginForm").on('submit', function(e){
		e.preventDefault();
		var pass = $("#loginEmail").checkEmail();
		$("#loginEmail").loginEmailTip(pass);
		if(pass) {
			var _userEmail = $('#loginEmail').val();
			var _userPassword = $('#loginPsw').val();
			//TODO: 加密
			$.post('login/validateuser', {
				useremail: _userEmail,
				password: _userPassword
			}, function(body) {
				if(body.success) {
					window.location = body.redirectUrl;
				} else {
					alert(body.msg);
				}
			});
		}
	});

	function resetForm(dom) {
		dom.find('input[type="text"], input[type="password"]').val('');
	}

	function validateRegForm() {
		return (($("#regPsw").val().length>=6)&&($("#regEmail").checkEmail())&&($("#regEmail").val()==$("#regConfirmEmail").val()));
	}
});
function refreshCheckCode(){ 
    $(".reg-checkCode").each(function(){ 
		$(this).unbind("click");
    	$(this).click(function(){
            var random=Math.random();
			$(this).attr({"src":"http://120.24.89.27:3000/getcaption?"+random});
		});
	})
}